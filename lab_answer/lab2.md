1. Apakah perbedaan antara JSON dan XML?
    - JSON berdasarkan bahasa JavaScript. XML berasal dari SGML (Standard Generalized Markup Language)
    - Data dalam JSON disimpan dalam bentuk pasangan key-value, sedangkan data dalam XML disimpan pada tag-tag tertentu yang dibuat
    - File JSON lebih mudah dibaca dibandingkan XML karena bisa langsung di-parse oleh fungsi JavaScript.
    - JSON tidak memerlukan sebuah end tag, sedangkan XML menggunakan start & end tag.

2. Apakah perbedaan antara HTML dan XML?
    - HTML merupakan suatu markup language yang telah ditentukan. XML menyediakan suatu framework untuk mendefinisikan sebuah markup language.
    - HTML hanya menampilkan informasi, sedangkan XML digunakan untuk menyimpan dan mentransport data.
    - Struktur HTML telah ditentukan, yaitu dengan penggunaan tag <head> dan <body>. Penggunaan tags-nya pun telah ditentukan dan memperbolehkan open tag. XML memiliki struktur yang disusun secara logis dengan menggunakan tags yang ditentukan user dan wajib menutup tag tersebut.
    - HTML tidak bersifat case-sensitive dan XML bersifat case-sensitive
