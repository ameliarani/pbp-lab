from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30, default=None)
    npm = models.CharField(max_length=10,default=None)
    dob = models.DateField(default=None)

    # TODO Implement missing attributes in Friend model
