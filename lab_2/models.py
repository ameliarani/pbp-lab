from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30, default=None)
    From = models.CharField(max_length=30, default=None)
    title = models.CharField(max_length=30, default=None)
    message = models.CharField(max_length=200, default=None) # TextField
     