from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    response = {'form' : form} 
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')
    else:
        return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)    
