import 'package:flutter/material.dart';

// void main() {
//   runApp(MaterialApp(
//     title: "Info Hotel Karantina",
//     home: HotelForm(),
//   ));
// }

class HotelForm extends StatefulWidget {
  @override
  _HotelFormState createState() => _HotelFormState();
}

class _HotelFormState extends State<HotelForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String? _Negara;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Info Hotel Karantina"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Contoh: Singapore",
                      labelText: "Nama Negara",
                      icon: Icon(Icons.flag),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (String? value) {
                      if (value!.isEmpty) {
                        return 'Nama negara tidak boleh kosong';
                      }
                      return null;
                    },
                    onSaved: (String? value) {
                      _Negara = value;
                    },
                  ),
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      return;
                    }
                    _formKey.currentState!.save();

                    print(_Negara);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
