import 'package:flutter/material.dart';
import 'package:lab_7/hotel_form_screen.dart';
// import './dropdown.dart';
// import 'package:getwidget/getwidget.dart';
// import './dummy_data.dart';
// import './screens/tabs_screen.dart';
// import './screens/meal_detail_screen.dart';
// import './screens/category_meals_screen.dart';
// import './screens/filters_screen.dart';
// import './screens/categories_screen.dart';
// import './models/meal.dart';
// import './models/hotel.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // List<Hotel> availableHotels = DUMMY_HOTELS;
  // String countryName = "";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.lightGreen,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
            appBar: AppBar(
                title: Text(
                  "Info Hotel Karantina",
                ),
                actions: <Widget>[
                  FlatButton(
                    textColor: Colors.white,
                    onPressed: () {},
                    child: Text("Beranda"),
                    shape: CircleBorder(
                        side: BorderSide(color: Colors.transparent)),
                  ),
                  FlatButton(
                    textColor: Colors.white,
                    onPressed: () {},
                    child: Text("Fitur"),
                    shape: CircleBorder(
                        side: BorderSide(color: Colors.transparent)),
                  ),
                  FlatButton(
                    textColor: Colors.white,
                    onPressed: () {},
                    child: Text("Halo, admin!"),
                    shape: CircleBorder(
                        side: BorderSide(color: Colors.transparent)),
                  ),
                ]),
            body: Center(
                child: Column(children: <Widget>[
              // Positioned(
              //   top: 0,
              //   right: 285,
              //   child: CircleAvatar(
              //     radius: 16,
              //     backgroundColor: Colors.red,
              //     foregroundColor: Colors.white,
              //     child: Text('24'),
              //   ),
              // ),
              // Positioned(top: 0.5, bottom: 0.5, left: 1.0, right: 1.0),
              FlatButton(
                onPressed: () {
                  Navigator.push(context, HotelForm());
                },
                child:
                    Text("Cari hotel karantina berdasarkan negara tujuanmu!"),
              ),
              Text("Info Hotel Karantina",
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold)),
              Text(
                "Temukan hotel karantina terbaik di negara tujuanmu! \nKenyamananmu adalah prioritas kami.\n",
                textAlign: TextAlign.center,
              ),
              Text("Cari hotel karantina di negara berikut:",
                  style: TextStyle(fontWeight: FontWeight.bold)),
              //Example(),
              Text(
                "Singapore | Malaysia | Thailand | Indonesia\n",
                textAlign: TextAlign.center,
              ),
              //const MyCustomForm(),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Image(
                        image: AssetImage('images/sg-fairmont.jpg'),
                      ),
                      title: Text("Fairmont Hotel"),
                      subtitle: Text("Singapore"),
                    ),
                  ],
                ),
              ),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Image(
                        image: AssetImage('images/my-putrajaya.jpg'),
                      ),
                      title: Text("Putrajaya Mariott Hotel"),
                      subtitle: Text("Malaysia"),
                    ),
                  ],
                ),
              ),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Image(
                        image: AssetImage('images/th-anantara.jpg'),
                      ),
                      title: Text("Anantara Siam Bangkok Hotel"),
                      subtitle: Text("Thailand"),
                    ),
                  ],
                ),
              ),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Image(
                        image: AssetImage('images/id-kempinski.jpg'),
                      ),
                      title: Text("Hotel Indonesia Kempinski"),
                      subtitle: Text("Indonesia"),
                    ),
                  ],
                ),
              ),
            ]))));
  }
}

class HotelForm extends MaterialPageRoute<StatelessWidget> {
  HotelForm()
      : super(builder: (BuildContext ctx) {
          return MaterialApp(
            title: 'Search Hotel',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: HotelForm(),
          );
        });
}
