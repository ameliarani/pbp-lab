import 'package:flutter/material.dart';

import './models/category.dart';
import './models/meal.dart';
import './models/hotel.dart';

const DUMMY_HOTELS = const [
  Hotel(
      id: '1',
      nama: 'Fairmont Singapore',
      imageUrl:
          'https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/70/16/701631_v4.jpeg',
      country: "Singapore"),
  Hotel(
      id: '2',
      nama: 'Mövenpick Hotel Sukhumvit',
      imageUrl:
          'https://cf.bstatic.com/images/hotel/max1024x768/221/221256920.jpg',
      country: "Thailand")
];
