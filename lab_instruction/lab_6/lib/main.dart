import 'package:flutter/material.dart';
import './dropdown.dart';
import 'package:getwidget/getwidget.dart';
// import './dummy_data.dart';
// import './screens/tabs_screen.dart';
// import './screens/meal_detail_screen.dart';
// import './screens/category_meals_screen.dart';
// import './screens/filters_screen.dart';
// import './screens/categories_screen.dart';
// import './models/meal.dart';
// import './models/hotel.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // List<Hotel> availableHotels = DUMMY_HOTELS;
  // String countryName = "";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.lightGreen,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
            appBar: AppBar(
                title: Text(
                  "Info Hotel Karantina",
                ),
                actions: <Widget>[
                  FlatButton(
                    textColor: Colors.white,
                    onPressed: () {},
                    child: Text("Beranda"),
                    shape: CircleBorder(
                        side: BorderSide(color: Colors.transparent)),
                  ),
                  FlatButton(
                    textColor: Colors.white,
                    onPressed: () {},
                    child: Text("Fitur"),
                    shape: CircleBorder(
                        side: BorderSide(color: Colors.transparent)),
                  ),
                  FlatButton(
                    textColor: Colors.white,
                    onPressed: () {},
                    child: Text("Halo, admin!"),
                    shape: CircleBorder(
                        side: BorderSide(color: Colors.transparent)),
                  ),
                ]),
            body: Center(
                child: Column(children: <Widget>[
              // Positioned(
              //   top: 0,
              //   right: 285,
              //   child: CircleAvatar(
              //     radius: 16,
              //     backgroundColor: Colors.red,
              //     foregroundColor: Colors.white,
              //     child: Text('24'),
              //   ),
              // ),
              // Positioned(top: 0.5, bottom: 0.5, left: 1.0, right: 1.0),
              Text("Info Hotel Karantina",
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold)),
              Text(
                "Temukan hotel karantina terbaik di negara tujuanmu! \nKenyamananmu adalah prioritas kami.\n",
                textAlign: TextAlign.center,
              ),
              Text("Cari hotel karantina di negara berikut:",
                  style: TextStyle(fontWeight: FontWeight.bold)),
              //Example(),
              Text(
                "Singapore | Malaysia | Thailand | Indonesia\n",
                textAlign: TextAlign.center,
              ),
              //const MyCustomForm(),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Image(
                        image: AssetImage('images/sg-fairmont.jpg'),
                      ),
                      title: Text("Fairmont Hotel"),
                      subtitle: Text("Singapore"),
                    ),
                  ],
                ),
              ),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Image(
                        image: AssetImage('images/my-putrajaya.jpg'),
                      ),
                      title: Text("Putrajaya Mariott Hotel"),
                      subtitle: Text("Malaysia"),
                    ),
                  ],
                ),
              ),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Image(
                        image: AssetImage('images/th-anantara.jpg'),
                      ),
                      title: Text("Anantara Siam Bangkok Hotel"),
                      subtitle: Text("Thailand"),
                    ),
                  ],
                ),
              ),
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Image(
                        image: AssetImage('images/id-kempinski.jpg'),
                      ),
                      title: Text("Hotel Indonesia Kempinski"),
                      subtitle: Text("Indonesia"),
                    ),
                  ],
                ),
              ),
              // Text("Cari hotel karantina di negara berikut:"),
              // Text("Singapore | Malaysia | Thailand | Indonesia"),
              // Row(children: <Widget>[
              //   RaisedButton(
              //     textColor: Colors.black,
              //     onPressed: () {
              //       {
              //         setState(() {
              //           countryName = "Singapore";
              //         });
              //       }
              //     },
              //     child: Text("Singapore"),
              //   ),
              //   RaisedButton(
              //     textColor: Colors.black,
              //     onPressed: () {
              //       {
              //         setState(() {
              //           countryName = "Thailand";
              //         });
              //       }
              //     },
              //     child: Text(
              //       "Thailand",
              //     ),
              //   ),
              // ]),
              // GFCard(
              //   boxFit: BoxFit.cover,
              //   titlePosition: GFPosition.start,
              //   title: GFListTile(
              //     avatar: GFAvatar(
              //         backgroundImage: NetworkImage(
              //             'https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/70/16/701631_v4.jpeg')),
              //     titleText: 'Fairmont Hotel',
              //     subTitleText: 'Singapore',
              //   ),
              //   //content: Text('Harga: Mulai dari 3600 SGD'),
              // ),
              // GFCard(
              //   boxFit: BoxFit.cover,
              //   titlePosition: GFPosition.start,
              //   title: GFListTile(
              //     avatar: GFAvatar(
              //         backgroundImage: AssetImage('images/my-putrajaya.jpg')),
              //     titleText: 'Putrajaya Marriott Hotel',
              //     subTitleText: 'Malaysia',
              //   ),
              //   //content: Text('Harga: Mulai dari 3600 SGD'),
              // ),
              // GFCard(
              //   boxFit: BoxFit.cover,
              //   titlePosition: GFPosition.start,
              //   title: GFListTile(
              //     avatar: GFAvatar(
              //         backgroundImage: AssetImage('images/th-anantara.jpg')),
              //     titleText: 'Anantara Siam Bangkok Hotel',
              //     subTitleText: 'Thailand',
              //   ),
              //   //content: Text('Harga: Mulai dari 3600 SGD'),
              // ),
              // GFCard(
              //   boxFit: BoxFit.cover,
              //   titlePosition: GFPosition.start,
              //   title: GFListTile(
              //     avatar: GFAvatar(
              //         backgroundImage: AssetImage('images/id-kempinski.jpg')),
              //     titleText: 'Hotel Indonesia Kempinski',
              //     subTitleText: 'Indonesia',
              //   ),
              //   //content: Text('Harga: Mulai dari 3600 SGD'),
              // ),
            ]))));
  }
}

// class Example extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => _ExampleState();
// }

// class _ExampleState extends State<Example> {
// //  List<String> _locations = ['Please choose a location', 'A', 'B', 'C', 'D']; // Option 1
// //  String _selectedLocation = 'Please choose a location'; // Option 1
//   List<String> _locations = ['A', 'B', 'C', 'D']; // Option 2
//   String _selectedLocation; // Option 2

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         body: Center(
//           child: DropdownButton(
//             hint:
//                 Text('Please choose a location'), // Not necessary for Option 1
//             value: _selectedLocation,
//             onChanged: (newValue) {
//               setState(() {
//                 _selectedLocation = newValue;
//               });
//             },
//             items: _locations.map((location) {
//               return DropdownMenuItem(
//                 child: new Text(location),
//                 value: location,
//               );
//             }).toList(),
//           ),
//         ),
//       ),
//     );
//   }
// }
// // String dropdownValue = 'One';

// @override
// Widget build(BuildContext context) {
//   return DropdownButton<String>(
//     value: dropdownValue,
//     icon: const Icon(Icons.arrow_downward),
//     iconSize: 24,
//     elevation: 16,
//     style: const TextStyle(
//       color: Colors.deepPurple
//     ),
//     underline: Container(
//       height: 2,
//       color: Colors.deepPurpleAccent,
//     ),
//     onChanged: (String? newValue) {
//       setState(() {
//         dropdownValue = newValue!;
//       });
//     },
//     items: <String>['One', 'Two', 'Free', 'Four']
//       .map<DropdownMenuItem<String>>((String value) {
//         return DropdownMenuItem<String>(
//           value: value,
//           child: Text(value),
//         );
//       })
//       .toList(),
//   );
// }

// class MyCustomForm extends StatefulWidget {
//   // const MyCustomForm({Key? key}) : super(key: key);

//   @override
//   MyCustomFormState createState() {
//     return MyCustomFormState();
//   }
// }

// class MyCustomFormState extends State<MyCustomForm> {
//   // Create a global key that uniquely identifies the Form widget
//   // and allows validation of the form.
//   //
//   // Note: This is a GlobalKey<FormState>,
//   // not a GlobalKey<MyCustomFormState>.
//   final _formKey = GlobalKey<FormState>();

//   @override
//   Widget build(BuildContext context) {
//     // Build a Form widget using the _formKey created above.
//     return Form(
//       key: _formKey,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           TextFormField(
//             // The validator receives the text that the user has entered.
//             validator: (value) {
//               if (value == null || value.isEmpty) {
//                 return 'Please enter some text';
//               }
//               return null;
//             },
//           ),
//           Padding(
//             padding: const EdgeInsets.symmetric(vertical: 16.0),
//             child: ElevatedButton(
//               onPressed: () {
//                 // Validate returns true if the form is valid, or false otherwise.
//                 if (_formKey.currentState.validate()) {
//                   // If the form is valid, display a snackbar. In the real world,
//                   // you'd often call a server or save the information in a database.
//                   ScaffoldMessenger.of(context).showSnackBar(
//                     const SnackBar(content: Text('Processing Data')),
//                   );
//                 }
//               },
//               child: const Text('Submit'),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
