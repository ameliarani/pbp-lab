import 'package:flutter/foundation.dart';

class Hotel {
  final String id;
  final String nama;
  final String imageUrl;
  final String country;

  const Hotel({
    @required this.id,
    @required this.nama,
    @required this.imageUrl,
    @required this.country,
  });
}
